require "ruboty/actions/github-statistics"

module Ruboty
  module Handlers
    class Blog < Ruboty::Handlers::Base
      on(/\bblog\z/, name: :stats, description: "Blog statistics for all users")
      on(/\bblog (?<user>\w+)\z/, name: :stats_by_user, description: "Blog statistics for user")
      on(/\bblog (?<range>\d{4}-\d{2}-\d{2}\:\d{4}-\d{2}-\d{2})\z/,
         name: :stats_by_range,
         description: "Statistics for range. ex: 2017-04-01:2017-04-30")
      on(/\bblog[-_]ranking\z/, name: :ranking, description: "Blog ranking")
      on(/\bblog[-_]ranking (?<range>\d{4}-\d{2}-\d{2}\:\d{4}-\d{2}-\d{2})/,
         name: :ranking_by_range,
         description: "Ranking by range. ex: 2017-04-01:2017-04-30")
      on(/\breload_blog\z/, name: :reload_stats, description: "Reload blog statistics")

      def register(message)
        github_user = user_for(message.from)
        url = message[:url]
        date = Date.parse(message[:date])
        line = "#{date.iso8601},#{github_user},#{url}\n"
        update_blog(date, line)
        message.reply("Registered: #{url}")
      rescue => ex
        message.reply("#{ex.class}: #{ex.message}")
        puts "#{ex.class}: #{ex.message}"
        puts ex.backtrace
      end

      def stats(message)
        action(message).stats
      end

      def stats_by_user(message)
        action(message).stats_by_user
      end

      def stats_by_range(message)
        action(message).stats_by_range
      end

      def ranking(message)
        action(message).ranking
      end

      def ranking_by_range(message)
        action(message).ranking_by_range
      end

      def reload_stats(message)
        action(message, force_reload: true).reload_stats
      end

      private

      def action(message, force_reload: false)
        Ruboty::Actions::GithubStatistics.new(
          message,
          "blog",
          force_reload: force_reload
        )
      end
    end
  end
end
