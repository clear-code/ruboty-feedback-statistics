require "octokit"
require "ruboty/actions/github"

module Ruboty
  module Handlers
    module Feedback
      class URL < Ruboty::Handlers::Base
        on(%r!(?<url>#{URI::ABS_URI_REF}) +(?<type>report|patch|help|find|info) +(?<upstream>\S+)(?: +(?<finder>\w+))?!,
           name: :register,
           description: "Register URL as a feedback")

        def register(message)
          build_action(message).call
        rescue => ex
          message.reply("#{ex.class}: #{ex.message}")
          puts "#{ex.class}: #{ex.message}"
          puts ex.backtrace
        end

        private

        def build_action(message)
          Ruboty::Actions::Github.new(message)
        end
      end
    end
  end
end
