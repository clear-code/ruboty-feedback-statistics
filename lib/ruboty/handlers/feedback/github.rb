require "csv"
require "ruboty/actions/github"

module Ruboty
  module Handlers
    module Feedback
      class Github < Base
        on(%r{(?<url>https://github\.com/.+?/.+?/pull/\d+)\z},
           name: :pull_request,
           description: "Register feedback to the project on GitHub.com")

        on(%r{(?<url>https://github\.com/.+?/.+?/issues/\d+)\z},
           name: :issue,
           description: "Register feedback to the project on GitHub.com")

        def pull_request(message)
          build_action(message).call
        rescue => ex
          message.reply("#{ex.class}: #{ex.message}")
          puts "#{ex.class}: #{ex.message}"
          puts ex.backtrace
        end

        def issue(message)
          build_action(message).call
        rescue => ex
          message.reply("#{ex.class}: #{ex.message}")
          puts "#{ex.class}: #{ex.message}"
          puts ex.backtrace
        end

        private

        def build_action(message)
          Ruboty::Actions::Github.new(message)
        end
      end
    end
  end
end
