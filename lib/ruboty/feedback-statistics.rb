require "octokit"
require "ruboty/env/github-env"

module Ruboty
  module FeedbackStatistics
    class Committer
      class DuplicateError < StandardError
      end

      include Ruboty::Env::GithubEnv

      def initialize
        @client = nil
      end

      def commit(date, line)
        path = File.join(statistics_directory, "#{date.strftime('%Y-%m')}.csv")
        begin
          response = client.contents(statistics_repository, path: path)
          sha = response.sha
          content = Base64.decode64(response.content)
          if content.lines.include?(line)
            raise DuplicateError
          end
          new_content = (content.lines + [line]).sort.join
          client.update_contents(statistics_repository,
                                 path,
                                 "Add feedback!!",
                                 sha,
                                 new_content)
        rescue Octokit::NotFound
          client.create_content(statistics_repository,
                                path,
                                "Add feedback!!",
                                line)
        end
      end

      private

      def client
        @client ||= Octokit::Client.new(access_token: access_token)
      end
    end
  end
end
