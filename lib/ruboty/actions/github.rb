require "octokit"
require "ruboty/env/github-env"
require "ruboty/feedback-statistics"

module Ruboty
  module Actions
    class Github < Ruboty::Actions::Base
      include Ruboty::Env::GithubEnv
      include Ruboty::FeedbackStatistics

      NAMESPACE = "feedback_github"

      attr_accessor :committer

      def initialize(message)
        super(message)
        @committer = committer
      end

      def call
        begin
          message[:type]
          have_type = true
        rescue IndexError
          have_type = false
        end
        if %r{\Ahttps://github\.com/(?<repo>.+?/.+?)/(?<type>(?:issues|pull))/(?<number>\d+)\z} =~ message[:url] && !have_type
          register(type: type, repo: repo, number: number)
        else
          register(type: message[:type])
        end
        message.reply("Registered: #{message[:url]}")
      rescue Committer::DuplicateError
        message.reply("Duplicate: #{message[:url]}")
      end

      private

      def client
        @client ||= Octokit::Client.new(access_token: access_token)
      end

      def register(type:, repo: nil, number: nil)
        case type
        when "pull"
          register_pull_request(repo, number)
        when "issues"
          register_issue(repo, number)
        else
          register_url
        end
        register_finder
      end

      def register_pull_request(repo, number)
        pull = client.pull_request(repo, number)
        date = pull.created_at.localtime.strftime("%Y-%m-%d")
        records[date] ||= []
        records[date] << {
          user: pull.user.login,
          upstream: pull.base.repo.full_name,
          type: :patch,
          url: pull.html_url
        }
        line = [
          date,
          pull.user.login,
          pull.base.repo.full_name,
          :patch,
          pull.html_url
        ].join(",") + "\n"
        update_statistics(pull.created_at.localtime, line)
      end

      def register_issue(repo, number)
        issue = client.issue(repo, number)
        date = issue.created_at.localtime.strftime("%Y-%m-%d")
        records[date] ||= []
        records[date] << {
          user: issue.user.login,
          upstream: repo,
          type: :report,
          url: issue.html_url
        }
        line = [
          date,
          issue.user.login,
          repo,
          :report,
          issue.html_url
        ].join(",") + "\n"
        update_statistics(issue.created_at.localtime, line)
      end

      def register_url
        github_user = user_for(message.from)
        type = message[:type]
        upstream = message[:upstream]
        url = message[:url]
        date = Date.today
        records[date] ||= []
        records[date] << {
          user: github_user,
          upstream: upstream,
          type: type,
          url: url
        }
        line = "#{date.iso8601},#{github_user},#{upstream},#{type},#{url}\n"
        update_statistics(date, line)
      end

      def register_finder
        begin
          github_user = message[:finder]
          return if github_user.nil? || github_user.empty?
          type = "find"
          upstream = message[:upstream]
          url = message[:url]
          date = Date.today
          records[date] ||= []
          records[date] << {
            user: github_user,
            upstream: upstream,
            type: type,
            url: url
          }
          line = "#{date.iso8601},#{github_user},#{upstream},#{type},#{url}\n"
          update_statistics(date, line)
        rescue IndexError
          # do nothing
        end
      end

      def robot
        message.robot
      end

      def records
        robot.brain.data[NAMESPACE] ||= {}
      end

      def update_statistics(date, line)
        @committer ||= Committer.new
        @committer.commit(date, line)
      end
    end
  end
end
