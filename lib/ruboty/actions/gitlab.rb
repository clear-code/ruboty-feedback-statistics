require "gitlab"
require "cgi/util"
require "ruboty/feedback-statistics"

module Ruboty
  module Actions
    class Gitlab < Ruboty::Actions::Base
      include Ruboty::FeedbackStatistics

      class UnknownType < StandardError
      end

      ENDPOINTS = {
        gitlab: "https://gitlab.com/api/v4",
        gnome: "https://gitlab.gnome.org/api/v4"
      }

      def call
        begin
          if %r{\Ahttps://gitlab\.com/(?<repo>.+?/.+?)/(?<type>(?:issues|merge_requests))/(?<number>\d+)\z} =~ message[:url]
            endpoint = ENDPOINTS[:gitlab]
            @gitlab_client = Client.new(endpoint)
            register(repo: repo, type: type, number: number)
          elsif %r{\Ahttps://gitlab\.gnome\.org/(?<repo>.+?/.+?)/(?<type>(?:issues|merge_requests))/(?<number>\d+)\z} =~ message[:url]
            endpoint = ENDPOINTS[:gnome]
            @gitlab_client = Client.new(endpoint)
            register(repo: repo, type: type, number: number)
          else
            # Unknown
          end
          message.reply("Registered: #{message[:url]}")
        rescue Commiter::DuplicateError
          message.reply("Duplicate: #{message[:url]}")
        rescue UnknownType => ex
          message.reply(ex.message)
        end
      end

      private

      def gitlab_client
        @gitlab_client
      end

      def register(repo:, type:, number:)
        case type
        when "merge_requets"
          register_merge_request(repo, number)
        when "issues"
          register_issue(repo, number)
        else
          raise UnknownType, "Unknown type: #{type}"
        end
      end

      def register_merge_request(repo, number)
        merge_request = @gitlab_client.merge_request(repo, number)
        created_at = Time.parse(merge_request.created_at)
        date = created_at.localtime.strftime("%Y-%m-%d")
        line = [
          date,
          merge_request.author.username,
          repo,
          :patch,
          merge_request.web_url
        ].join(",") + "\n"
        update_statistics(created_at.localtime, line)
      end

      def register_issue(repo, number)
        issue = @gitlab_client.issue(repo, number)
        created_at = Time.parse(issue.created_at)
        date = created_at.localtime.strftime("%Y-%m-%d")
        line = [
          date,
          issue.author.username,
          repo,
          :report,
          issue.web_url
        ].join(",") + "\n"
        update_statistics(created_at.localtime, line)
      end

      def update_statistics(date, line)
        @committer ||= Committer.new
        @committer.commit(date, line)
      end
    end

    # Unauthenticated client for gitlab
    class Client
      include CGI::Util

      def initialize(endpoint)
        @endpoint = endpoint
        @client = ::Gitlab.client(endpoint: endpoint)
      end

      def issue(repo, number)
        @client.get("/projects/#{escape(repo)}/issues/#{number}", unauthenticated: true)
      end

      def merge_request(repo, number)
        @client.get("/projects/#{escape(repo)}/merge_requests/#{number}", unauthenticated: true)
      end
    end
    
  end
end
