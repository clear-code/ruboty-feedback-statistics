# ruboty-feedback-statistics

A [Zulip](https://zulipchat.com/) bot which helps aggregating free software feedback activity of your team.

## Quick start (on Debian or Ubuntu)

```
$ sudo apt install redis-server
$ git clone https://gitlab.com/clear-code/ruboty-feedback-statistics.git
$ cd ruboty-feedback-statistics
$ bundle install --path vendor/bundle
$ cp env.example .env
(edit it)
$ bundle exec ruboty --dotenv --load bot.rb
```
